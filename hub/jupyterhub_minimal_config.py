import os

from jupyter_client.localinterfaces import public_ips

c.JupyterHub.authenticator_class = 'oidcauthenticator.OIDCAuthenticator'
c.OIDCAuthenticator.username_key = 'preferred_username'
c.OIDCAuthenticator.scope = ['openid', 'profile', 'offline_access']
c.OIDCAuthenticator.allowed_roles = []
c.OIDCAuthenticator.check_signature=True
c.OIDCAuthenticator.verify_aud=True
c.OIDCAuthenticator.allowed_audience=os.environ["OAUTH_ALLOWED_AUDIENCES"].split(",")
c.OIDCAuthenticator.exchange_tokens = os.environ["EXCHANGE_TOKENS"].split(",")
c.OIDCAuthenticator.jwt_signing_algorithms = ["RS256"]

c.OIDCAuthenticator.oidc_issuer = os.environ["OIDC_ISSUER"]
c.OIDCAuthenticator.enable_auth_state = True

# The docker instances need access to the Hub, so the default loopback port doesn't work:
c.JupyterHub.hub_ip = public_ips()[0]

# spawn with Docker
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'

network_name = "hub_network"
c.DockerSpawmer.use_internal_ip = True
c.DockerSpawner.network_name = network_name
c.DockerSpawner.extra_host_config = { 'network_mode': network_name }
